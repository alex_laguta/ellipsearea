﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace EllipseArea
{
    class MainViewModel : INotifyPropertyChanged 
    {
        public CancellationTokenSource cts;
        public WriteableBitmap wb;
        private MainWindow mainWindow;

        private double radiusA = 30;
        private double radiusB = 20;
        private double epsilon = 0.01;
        private int threadsCount = 1;

        public double RadiusA
        {
            get { return radiusA; }
            set
            {
                if (radiusA != value)
                {
                        radiusA = value;
                        OnPropertyChanged("RadiusA");
                }
            }
        }

        public double RadiusB
        {
            get { return radiusB; }
            set
            {
                if (radiusB != value)
                {
                    radiusB = value;
                    OnPropertyChanged("RadiusB");
                }
            }
        }

        public double Epsilon
        {
            get { return epsilon; }
            set
            {
                if (epsilon != value)
                {
                    epsilon = value;
                    OnPropertyChanged("Epsilon");
                }
            }
        }

        public int ThreadsCount
        {
            get { return threadsCount; }
            set
            {
                if (threadsCount != value)
                {
                    threadsCount = value;
                    OnPropertyChanged("ThreadsCount");
                }
            }
        }


        public MainViewModel()
        {
            mainWindow = MainWindow.AppWindow;
        }

        public double CalculateArea(double a, double b, double epsilon)
        {
            int mSuccess = 0;
            int nAll = 0;
            double rectangleArea = 4 * a * b;
            double ellipseArea = 0;
            cts = new CancellationTokenSource();

            try
            {
                if (epsilon > 1.0)
                    throw new ArgumentException("Epsilon is invalid, must be less than 1.0!");
            }
            catch(ArgumentException exception)
            {
                MessageBox.Show(exception.Message, "Argument Exception");
                return -1;
            }

            int n = (int)Math.Ceiling(Math.Pow((1.0 / epsilon), 2.0));

            ThreadPool.SetMaxThreads(threadsCount, threadsCount);
            var e = new CountdownEvent(n);
            Random rand = new Random(DateTime.Now.Millisecond);

            for (int x = 0; x < n; x++)
            {
                ThreadPool.QueueUserWorkItem(delegate(object state)
                {
                    object[] array = state as object[];
                    CancellationToken token = (CancellationToken)array[0];

                    if (token.IsCancellationRequested)
                    {
                        e.Signal();
                        return;
                    }

                    bool inEllipse = false;
                    double aCopy = Convert.ToDouble(array[1]);
                    double bCopy = Convert.ToDouble(array[2]);    
                    Random random = (Random)array[3];

                    double ellipseX = 2 * aCopy * random.NextDouble();
                    double ellipseY = 2 * bCopy * random.NextDouble();

                    if (Math.Pow((ellipseX - aCopy), 2) / Math.Pow(aCopy, 2) + Math.Pow((ellipseY - bCopy), 2) / Math.Pow(bCopy, 2) <= 1.0)
                    {
                        inEllipse = true;
                        Interlocked.Increment(ref mSuccess);
                    }
                    Interlocked.Increment(ref nAll);

                    Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate
                    {
                        byte[] colorData = { 0, 0, 0, 255 };
                        if (inEllipse)
                            colorData[1] = 255;
                        else
                            colorData[2] = 255;

                        Int32Rect rect = new Int32Rect((int)ellipseX + 20, (int)ellipseY + 20, 1, 1);
                        int stride = (wb.PixelWidth * wb.Format.BitsPerPixel) / 8;

                        wb.Lock();
                        wb.WritePixels(rect, colorData, stride, 0);
                        wb.AddDirtyRect(rect);
                        wb.Unlock();
                       
                    }));

                    e.Signal();

                }, new object[] { cts.Token, a, b, rand });
            }

            e.Wait();

            e.Dispose();
            cts.Dispose();

            ellipseArea = ((double)mSuccess / nAll) * rectangleArea;
            return ellipseArea;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
