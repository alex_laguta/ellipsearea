﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace EllipseArea
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainViewModel vm;
        public static MainWindow AppWindow;

        public MainWindow()
        {
            InitializeComponent();
            AppWindow = this;
            vm = new MainViewModel();
            DataContext = vm;
        }

        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            canvasEllipse.Children.Clear();
            
            double a, b, epsilon;
            Double.TryParse(textEllipseA.Text, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out a);
            Double.TryParse(textEllipseB.Text, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out b);
            Double.TryParse(textEpsilon.Text, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out epsilon);

            canvasEllipse.Width = 40 + 2 * a;
            canvasEllipse.Height = 40 + 2 * b;
            SizeToContent = SizeToContent.WidthAndHeight;

            WriteableBitmap wb = new WriteableBitmap((int)canvasEllipse.Width, (int)canvasEllipse.Height, 96, 96, PixelFormats.Bgra32, null);
            pixelsImage.Height = canvasEllipse.Height;
            pixelsImage.Width = canvasEllipse.Width;
            pixelsImage.Source = wb;
            vm.wb = wb;
            canvasEllipse.Children.Add(pixelsImage);

            Task<double> calculate = new Task<double>(() => {
                return vm.CalculateArea(a, b, epsilon);
            });

            Task showMessageBox = calculate.ContinueWith(result =>
            {
                if (result.Result != -1.0)
                    MessageBox.Show(String.Format("Ellipse Area = {0}", result.Result.ToString()), "Ellipse Area");
            });

            Task buttonsChange = showMessageBox.ContinueWith(delegate
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate
                {
                    buttonStart.IsEnabled = true;
                    buttonStop.IsEnabled = false;
                }));
            });

            calculate.Start();

            buttonStart.IsEnabled = false;
            buttonStop.IsEnabled = true;
        }

        private void buttonStop_Click(object sender, RoutedEventArgs e)
        {
            vm.cts.Cancel();
        }

        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                vm.cts.Cancel();
            }
            catch (ObjectDisposedException exception)
            {

            }
            catch (NullReferenceException exception)
            {

            }
            Application.Current.Shutdown();
        }

    }
}
