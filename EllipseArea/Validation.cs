﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace EllipseArea
{

    public class RadiusRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
                                                    CultureInfo culture)
        {
            double radius;
            if (!double.TryParse((string)value, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out radius))
            {
                return new ValidationResult(false, "The value is not numeric");
            }
            if (radius <= 0)
            {
                return new ValidationResult(false, "The value must be > 0");
            }
            return new ValidationResult(true, null);
        }
    }

    public class EpsilonRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
                                                    CultureInfo culture)
        {
            double epsilon;
            if (!Double.TryParse((string)value, NumberStyles.Number, CultureInfo.CreateSpecificCulture("en-US"), out epsilon))
            {
                return new ValidationResult(false, "The value is not numeric");
            }
            if (epsilon <= 0 || epsilon > 1.0)
            {
                return new ValidationResult(false, "The value must be greater than 0 and less than 1.0 ");
            }
            return new ValidationResult(true, null);
        }
    }

    public class ThreadsRule : ValidationRule
    {
        public override ValidationResult Validate(object value,
                                                    CultureInfo culture)
        {
            uint threads;
            if (!UInt32.TryParse((string)value, out threads))
            {
                return new ValidationResult(false, "The value is not positive integer");
            }
            if (threads == 0)
            {
                return new ValidationResult(false, "The value must be > 0");
            }
            return new ValidationResult(true, null);
        }
    }


}
